# DMG-Wrapper

This is analogous to the DMG Wrapping done by the CI at the [client repo](https://gitlab.com/sheepitrenderfarm/client/-/tree/master/dmg-wrapper).  
Building requires Docker and Internet connection to download the latest SheepIt Jar

## Wrapping DMGs

Run the following
```bash
sudo ./run.sh
```

The docker logs will show how to copy out the built DMGs out of the container.  
To rerun this script, it requires the removal of the previous container via `docker rm dmg-wrapper`.

## Artifacts

The build process will generate:

```
SheepIt-Apple-ARM-Full.dmg
SheepIt-Apple-ARM-Lite.dmg
SheepIt-Intel-x64-Full.dmg
SheepIt-Intel-x64-Lite.dmg
SheepIt-Slim.dmg
```
 The properties are as follows:
 
| Name                       | Runs on ARM | Runs on x64 | Includes JRE | Includes JAR | Size   |
|----------------------------|-------------|-------------|--------------|--------------|--------|
| SheepIt-Apple-ARM-Full.dmg | X           |             | X            | X            | ~60 MB |
| SheepIt-Apple-ARM-Lite.dmg | X           |             | X            |              | ~50 MB |
| SheepIt-Intel-x64-Full.dmg |             | X           | X            | X            | ~55 MB |
| SheepIt-Intel-x64-Lite.dmg |             | X           | X            |              | ~45 MB |
| SheepIt-Slim.dmg           | X           | X           |              |              | ~1 MB  |

Any JAR or JRE that is not included is loaded and cached from the internet and also auto-updated on every launch.

#### Beta

The Lite and Slim packages can be instructed to use the Beta of the SheepIt jar via an empty file like so:

```bash
mkdir -p "$HOME/Library/Application Support/SheepIt Launcher"
touch "$HOME/Library/Application Support/SheepIt Launcher/JOINBETA"
```

## Other files

### Dockerfile and build-dmg.sh

Dockerimage that runs `build-dmg.sh` to generate the DMG's.

#### dmg and build-libdmg-hfsplus.sh

The `dmg` executable was generated from a stripped down [libdmg-hfsplus](https://github.com/fanquake/libdmg-hfsplus) using the `build-lbdmg-hfsplus.sh` script.

### OpenJDK Tars

The JREs that are used during the build process.  
Apple uses the Java 17 JRE because that was the only one available from [Adoptium](https://adoptium.net/) at the time of writing.

### SheepIt.png and SheepIt.icns

SheepIt.png used for the Dock icon during the launch via `-Xdock:icon=./$APP.png` JRE argument.  
SheepIt.icns used as the Icon pack for the SheepIt.app executable.

### SheepIt.tar.gz and SheepIt executable

An archive generated on a Mac, a tree view with comments below:

```bash
├──   -> /Applications                  # Symbolic Link (without a name) to Applications directory for easy installation
├── ._.background                       # ._. files are generally extended attributes for MacOS (*sigh*)
├── .background                         # Hidden folder for the ...
│    └── background.png                 # ... DMG background
├── ._.DS_Store
├── .DS_Store                           # Desktop Services Store file, holds info about icon size, icon locations, folder background
├── ._SheepIt.app
└── SheepIt.app                         # Yes, a folder is an executable. Don't ask, it's MacOS
    ├── Contents
    │         ├── MacOS                 # SheepIt.app entrypoint for anything named SheepIt, location soon for SheepIt.sh
    │         └── Resources             # JREs and JARs might live here
    │             └── client            # Folder for everything launcher related
    ├── ._Icon\015                      # The SheepIt.icns (dragged in MacOS onto the SheepIt.app under right-click -> Get Info)
    └── Icon\015                        # and yes, that is a carriage return (\r) at the end. Again, it's MacOS
```

#### SheepIt.sh

Just launches a new Terminal and executes `SheepIt-Run.sh`,  
the one copied into the `Resources/client` directory during the build process.

The Terminal window gets closed on a non-error exit status in the `SheepIt-Run.sh`  
via `osascript -e 'tell application "Terminal" to close (every window whose name contains "SheepIt-Run.sh")' &`.