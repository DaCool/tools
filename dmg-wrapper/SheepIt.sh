#!/bin/bash

cd "${0%/*}"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

if [ -e "../Resources/client/DEBUG" ]; then
    set -x
fi

VER=$(sw_vers -productVersion | cut -d'.' -f1)
if [ "$VER" -lt 10 ]; then
	osascript -e 'display alert "SheepIt unsupported" message "SheepIt is not supported on this version of MacOS." as critical'
	exit 1
elif [ "$VER" -eq 10 ]; then
    # I do not know why it does not work if this is not on a single line, it maybe because of MacOS, their horrible osascript or the ancient bash they ship, if you can do better, feel free to make an MR
	if osascript -e 'display alert "SheepIt unsupported" message "SheepIt only works with a workaround on this version of MacOS. For more info visit https://gitlab.com/DaCoolX/sheepit-tools/-/tree/DaCool-No-Fuzz/dmg-wrapper" buttons { "Cancel", "Open URL" } cancel button "Cancel" as critical'; then
		open "https://gitlab.com/DaCoolX/sheepit-tools/-/tree/DaCool-No-Fuzz/dmg-wrapper"
	fi
	exit 1
fi

open -a Terminal.app ../Resources/client/SheepIt-Run.sh
