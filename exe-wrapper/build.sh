#!/bin/bash
set -euo pipefail # Unoffical strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "$(dirname "$(readlink -f "$0")")"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

# jvm is taken from https://adoptium.net/releases.html?variant=openjdk11&jvmVariant=hotspot
JVM_NAME="jdk-11.0.13+8-jre"
# Creating folder structure
mkdir /build/jre
# Downloading client to the target directory
wget https://www.sheepit-renderfarm.com/media/applet/client-latest.php -O jre/sheepit-client.jar
# Unziping jre
unzip "$JVM_NAME".zip
cp -rf "$JVM_NAME"/* jre/ # Copy JRE to be packaged
# Compressing app package
cd /build/jre
7zr a -mx=9 /build/application.7z ./
# Building the exe bundle and cleaning up
cd /build
cat starter.sfx config.cfg application.7z > sheepit-wrapper.exe
rm -rf application.7z jre $JVM_NAME
echo "You may copy the sheepit-wrapper.exe now from the container at /build/sheepit-wrapper.exe"