# How to for installing a project mirror with Traefik

server: **static-tx5-usa.sheepit-renderfarm.com**

local storage on the server: **/srv**


1. apt-get install docker docker-compose
2. mkdir /srv/traefik
3. mkdir /srv/static-tx5-usa
4. copy this contents into /srv/traefik/docker-composer.yml
<pre>version: '3.6'

services:
  traefik:
    image: traefik:2.3
    container_name: traefik
    network_mode: host
    restart: always
    command:
      - "--providers.docker=true"
      - "--providers.docker.exposedByDefault=false"
      - "--entryPoints.http.address=:80"
      - "--entryPoints.http.http.redirections.entryPoint.to=https"
      - "--entryPoints.https.address=:443"
      - "--certificatesResolvers.traefik.acme.email=admin@sheepit-renderfarm.com"
      - "--certificatesResolvers.traefik.acme.storage=/etc/traefik/acme.json"
      - "--certificatesResolvers.traefik.acme.httpChallenge=true"
      - "--certificatesResolvers.traefik.acme.httpChallenge.entryPoint=http"
    volumes:
      - /srv/traefik/config:/etc/traefik
      - /var/run/docker.sock:/var/run/docker.sock:ro
    labels:
      traefik.enable: true
</pre>
5. copy this contents into /srv/static-tx5-usa/docker-composer.yml
<pre>version: '3.6'
services:

  project-mirror:
    image: sheepitrenderfarm/project-mirror:6.0
    restart: always
    volumes:
      - /srv/static-tx5-usa/storage:/tmp/projects
    labels:
      traefik.enable: true
      traefik.http.services.project-mirror.loadbalancer.server.port: 80
      traefik.http.routers.project-mirror.entrypoints: https
      traefik.http.routers.project-mirror.rule: Host(`static-tx5-usa.sheepit-renderfarm.com`)
      traefik.http.routers.project-mirror.tls: true
      traefik.http.routers.project-mirror.tls.certresolver: traefik
</pre>
6. **Optional**, if you want to limit the storage capacity to 100GB
  *  create a file /srv/static-tx5-usa/etc/config.inc.local.php with this content:
```
<?php
$config['storage']['max'] = 100000000000; // size in bytes
?>
```
  * modify /srv/static-tx5-usa/docker-composer.yml by adding this line on under volumes part.
<pre>
- /srv/static-tx5-usa/etc:/etc/sheepit-mirror
</pre>
7. cd /srv/traefik
8. docker-composer up -d
9. cd /srv/static-tx5-usa
10. docker-composer up -d
10a. if you already have a storage directory */srv/static-tx5-usa/storage* set the permissions to www-data
11. To verify every is ok, go on https://static-tx5-usa.sheepit-renderfarm.com/status.php





